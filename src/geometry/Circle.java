package geometry;

public class Circle implements Shape {
	private double radius;
	
	public Circle(double radius) {
		
		if(radius <= 0) {
			
			throw new IllegalArgumentException("radius must greater than zero");
			
		}
		this.radius = radius;
	}
	
	public double getRadius() {
		return radius;
	}
	
	public double getArea() {
		
		return Math.PI*radius*radius;
	}
	
	public double getPerimeter() {
		
		return 2*Math.PI*radius;
	}

}
