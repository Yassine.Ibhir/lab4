package inheritance;

public class Book {
	protected String title;
	private String author;

	
	public Book(String title, String author) {
		
		if(title.length() == 0 || author.length() == 0) { 
			throw new IllegalArgumentException("You need to provide valid inputs");
		}
		this.title = title;
		this.author = author;
	}



	public String getTitle() {
		return title;
	}
	
	public String getAuthor() {
		return author;
	}
	
	//override
	public String toString() {
		return "title: "+ title + ", author: "+ author;
		
	}

}
